<?php

/**
 * @file
 * Hooks provided by the Responsive Share Buttons module.
 */

/**
 * Alters an individual sharing link.
 *
 * @param string $link_text
 *   The text that will be used in the share link.
 * @param string $link_url
 *   The URL that will be used to share content.
 * @param array $link_options
 *   The options array that will be passed to l().
 *
 * @param string $network
 *   The name of the social network
 */
function hook_responsive_share_buttons_share_link_alter(&$link_text, &$link_url, &$link_options, $network) {

}

/**
 * Alters the list of sharing links.
 *
 * @param array $links
 *   The array of links that will appear.
 */
function hook_responsive_share_buttons_links_alter(&$links) {

}
