(function ($) {
  Drupal.behaviors.rsb = {
    attach: function (context, settings) {
      var pageTitle = document.title; //HTML page title
      var pageUrl = location.href; //Location of the page      

      //var animateSpeed = Drupal.settings.responsive_share_buttons.animateSpeed;
      //user hovers on the share button	
      $('#share-wrapper .share-inner-wrp li').hover(function () {
        var hoverEl = $(this); //get element
        //browsers with width > 699 get button slide effect
        if ($(window).width() > 699) {
          if (hoverEl.hasClass('visible')) {
            hoverEl.stop(true);
            hoverEl.animate({"margin-left": "-117px"}, "fast").removeClass('visible');
          }
          else {
            hoverEl.stop(true);
            hoverEl.animate({"margin-left": "0px"}, "fast").addClass('visible');
          }
        }
      });

      $('.button-wrap').click(function (event) {

        //Parameters for the Popup window
        winWidth = 650;
        winHeight = 450;
        winLeft = ($(window).width() - winWidth) / 2,
          winTop = ($(window).height() - winHeight) / 2,
          winOptions = 'width=' + winWidth + ',height=' + winHeight + ',top=' + winTop + ',left=' + winLeft;

        //open Popup window and redirect user to share website.
        window.open($(this).attr('href'), 'Share This Link', winOptions);
        return false;
      });
    },
  }
})(jQuery);