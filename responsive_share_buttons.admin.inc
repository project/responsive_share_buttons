<?php

/**
 * @file
 * Functionality for Responsive share buttons administration.
 */

/**
 * Settings form as implemented by hook_menu.
 */
function responsive_share_buttons_settings($form, &$form_state) {
  $form['responsive_share_buttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Responsive Buttons'),
    '#tree' => TRUE,
    '#theme' => 'responsive_share_buttons_settings',
  );

  // Get the current networks, sorted by weight.
  $networks = variable_get('responsive_share_buttons', array());
  uasort($networks, 'drupal_sort_weight');

  foreach ($networks as $name => $network) {

    $form['responsive_share_buttons']['networks'][$name]['active'] = array(
      '#type' => 'checkbox',
      '#title' => $name,
      '#default_value' => isset($network['active']) ? $network['active'] : FALSE,
    );

    $form['responsive_share_buttons']['networks'][$name]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => isset($network['weight']) ? $network['weight'] : 0,
      '#attributes' => array('class' => array('item-row-weight')),
    );
  }

  $form['responsive_share_buttons_twitter_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter username'),
    '#default_value' => variable_get('responsive_share_buttons_twitter_name', ''),
    '#description' => t('Add a twitter name for tweets to include a via mention'),
  );

  $form['#validate'][] = 'responsive_share_buttons_settings_validate';

  return system_settings_form($form);
}

/**
 * Validation callback for responsive_share_buttons_settings.
 *
 * Sorts the networks by weight.
 */
function responsive_share_buttons_settings_validate(&$form, &$form_state) {
  $networks = variable_get('responsive_share_buttons', array());

  // Replace the input because the variable name and input name don't match.
  $values = $form_state['values']['responsive_share_buttons']['networks'];
  unset($form_state['values']['responsive_share_buttons']['networks']);

  foreach ($networks as $name => $network) {
    // Structure the data so it can be sorted.
    $form_state['values']['responsive_share_buttons'][$name] = array(
      'active' => $values[$name]['active'],
      'weight' => $values[$name]['weight'],
    );
  }
}
